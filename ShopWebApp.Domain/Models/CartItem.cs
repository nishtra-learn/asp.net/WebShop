﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopWebApp.Domain.Models
{
    public class CartItem
    {
        public Good Good { get; set; }
        public int Count { get; set; }
    }
}
