﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopWebApp.Domain.Models
{
    public class Cart
    {
        private List<CartItem> items = new List<CartItem>();
        public IEnumerable<CartItem> Items => items;


        public void AddItem(Good good, int quantity)
        {
            var existingItem = items.FirstOrDefault(t => t.Good.Id == good.Id);
            if (existingItem == null)
            {
                items.Add(new CartItem { Good = good, Count = quantity });
            }
            else
            {
                existingItem.Count += quantity;
            }
        }


        public void RemoveItem(Good good)
        {
            items.RemoveAll(t => t.Good.Id == good.Id);
        }


        public void Clear()
        {
            items.Clear();
        }


        public double GetTotalPrice()
        {
            return items.Sum(t => t.Good.Price * t.Count);
        }

    }
}
