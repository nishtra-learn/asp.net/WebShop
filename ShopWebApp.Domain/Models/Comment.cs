﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApp.Domain.Models
{
    public class Comment : Interfaces.IEntity
    {
        public int Id { get; set; }
        public string CommentText { get; set; }
        public string UserName { get; set; }
        
        public int GoodId { get; set; }
        public virtual Good Good { get; set; }
    }
}