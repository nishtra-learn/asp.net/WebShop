﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopWebApp.Domain.Interfaces
{
    /**
     * Simplified repository interface to use with UnitOfWork pattern
     */
    public interface IUoFRepository<T>
        where T : IEntity
    {
        IEnumerable<T> GetList();
        T GetById(int id);
        void Delete(int id);
        void Delete(T entity);
        void Update(T entity);
        void Create(T entity);
    }
}
