﻿using ShopWebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopWebApp.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUoFRepository<Category> Categories { get; }
        IUoFRepository<Good> Goods { get; }
        IUoFRepository<Comment> Comments { get; }

        void SaveChanges();
    }
}
