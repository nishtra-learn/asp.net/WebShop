﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopWebApp.Domain.Interfaces
{
    /**
     * Repository interface to use without UnitOfWork pattern
     */
    public interface IRepository<T> : IDisposable
        where T : IEntity
    {
        IEnumerable<T> GetList();
        T GetById(int id);
        bool Delete(int id);
        bool Delete(T entity);
        T Update(T entity);
        T Create(T entity);
    }
}
