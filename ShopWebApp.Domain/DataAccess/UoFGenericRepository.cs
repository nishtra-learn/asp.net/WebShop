﻿using ShopWebApp.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopWebApp.Domain.DataAccess
{
    public class UoFGenericRepository<T> : IUoFRepository<T>
        where T : class, IEntity
    {
        private ShopContext db;

        public UoFGenericRepository(ShopContext dbContext)
        {
            db = dbContext;
        }

        public void Create(T entity)
        {
            db.Set<T>().Add(entity);
        }

        public void Delete(int id)
        {
            var itemToDelete = db.Set<T>().Find(id);

            if (itemToDelete == null)
                return;

            db.Set<T>().Remove(itemToDelete);
        }

        public void Delete(T entity)
        {
            db.Set<T>().Remove(entity);
        }

        public T GetById(int id)
        {
            return db.Set<T>().Find(id);
        }

        public IEnumerable<T> GetList()
        {
            return db.Set<T>().AsEnumerable();
        }

        public void Update(T entity)
        {
            db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}
