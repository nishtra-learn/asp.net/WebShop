﻿using ShopWebApp.Domain.Models;
using ShopWebApp.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApp.Domain.DataAccess
{
    public class GoodsRepository : IRepository<Good>
    {
        private ShopContext db;
        private bool disposedValue;

        public GoodsRepository(string connectionString)
        {
            db = new ShopContext(connectionString);
        }

        public Good Create(Good entity)
        {
            db.Goods.Add(entity);
            db.SaveChanges();
            return entity;
        }

        public bool Delete(int id)
        {
            var itemToDelete = db.Goods.Find(id);

            if (itemToDelete == null)
                return false;

            db.Goods.Remove(itemToDelete);
            db.SaveChanges();
            return true;
        }

        public bool Delete(Good entity)
        {
            if (entity.Id == 0)
                return false;

            db.Goods.Remove(entity);
            db.SaveChanges();
            return true;
        }

        public Good GetById(int id)
        {
            return db.Goods.Find(id);
        }

        public IEnumerable<Good> GetList()
        {
            return db.Goods.ToList();
        }

        public Good Update(Good entity)
        {
            db.Entry(entity).State = entity.Id == 0
                ? System.Data.Entity.EntityState.Added
                : System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return entity;
        }

        #region Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    db.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~GoodsRepository()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}