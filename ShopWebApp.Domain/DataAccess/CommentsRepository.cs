﻿using ShopWebApp.Domain.Models;
using ShopWebApp.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApp.Domain.DataAccess
{
    public class CommentsRepository : IRepository<Comment>
    {
        private ShopContext db;
        private bool disposedValue;

        public CommentsRepository(string connectionString)
        {
            db = new ShopContext(connectionString);
        }

        public Comment Create(Comment entity)
        {
            db.Comments.Add(entity);
            db.SaveChanges();
            return entity;
        }

        public bool Delete(int id)
        {
            var itemToDelete = db.Comments.Find(id);

            if (itemToDelete == null)
                return false;

            db.Comments.Remove(itemToDelete);
            db.SaveChanges();
            return true;
        }

        public bool Delete(Comment entity)
        {
            if (entity.Id == 0)
                return false;

            db.Comments.Remove(entity);
            db.SaveChanges();
            return true;
        }

        public Comment GetById(int id)
        {
            return db.Comments.Find(id);
        }

        public IEnumerable<Comment> GetList()
        {
            return db.Comments.ToList();
        }

        public Comment Update(Comment entity)
        {
            db.Entry(entity).State = entity.Id == 0
                ? System.Data.Entity.EntityState.Added
                : System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return entity;
        }

        #region Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    db.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~CommentsRepository()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}