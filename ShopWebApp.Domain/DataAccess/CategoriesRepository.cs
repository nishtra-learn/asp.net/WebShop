﻿using ShopWebApp.Domain.Models;
using ShopWebApp.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApp.Domain.DataAccess
{
    public class CategoriesRepository : IRepository<Category>
    {
        private ShopContext db;
        private bool disposedValue;

        public CategoriesRepository(string connectionString)
        {
            db = new ShopContext(connectionString);
        }

        public Category Create(Category entity)
        {
            db.Categories.Add(entity);
            db.SaveChanges();
            return entity;
        }

        public bool Delete(int id)
        {
            var itemToDelete = db.Categories.Find(id);

            if (itemToDelete == null)
                return false;

            db.Categories.Remove(itemToDelete);
            db.SaveChanges();
            return true;
        }

        public bool Delete(Category entity)
        {
            if (entity.Id == 0)
                return false;

            db.Categories.Remove(entity);
            db.SaveChanges();
            return true;
        }

        public Category GetById(int id)
        {
            return db.Categories.Find(id);
        }

        public IEnumerable<Category> GetList()
        {
            return db.Categories.ToList();
        }

        public Category Update(Category entity)
        {
            db.Entry(entity).State = entity.Id == 0
                ? System.Data.Entity.EntityState.Added
                : System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return entity;
        }

        #region Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    db.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~CategoriesRepository()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}