﻿using ShopWebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ShopWebApp.Domain.DataAccess
{
    public class ShopContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Good> Goods { get; set; }


        public ShopContext()
        { }

        public ShopContext(string connectionString) : base(connectionString)
        { }
    }
}