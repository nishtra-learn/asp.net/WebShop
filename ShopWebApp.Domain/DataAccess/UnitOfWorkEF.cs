﻿using ShopWebApp.Domain.Interfaces;
using ShopWebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopWebApp.Domain.DataAccess
{
    public class UnitOfWorkEF : IUnitOfWork
    {
        ShopContext db;
        public UnitOfWorkEF(string connectionString)
        {
            this.db = new ShopContext(connectionString);
        }

        private IUoFRepository<Category> categories;
        private IUoFRepository<Good> goods;
        private IUoFRepository<Comment> comments;
        public IUoFRepository<Category> Categories { get => (categories = categories ?? new UoFGenericRepository<Category>(db)); }
        public IUoFRepository<Good> Goods { get => (goods = goods ?? new UoFGenericRepository<Good>(db)); }
        public IUoFRepository<Comment> Comments { get => (comments = comments ?? new UoFGenericRepository<Comment>(db)); }


        #region Dispose
        private bool disposedValue = false;

        public void SaveChanges()
        {
            db.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~UnitOfWorkEF()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
