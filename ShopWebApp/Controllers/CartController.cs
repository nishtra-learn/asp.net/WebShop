﻿using ShopWebApp.Domain.Interfaces;
using ShopWebApp.Domain.Models;
using ShopWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopWebApp.Controllers
{
    public class CartController : Controller
    {
        private IUnitOfWork db;


        public CartController(IUnitOfWork dbUnit)
        {
            this.db = dbUnit;
        }


        // GET: Cart
        public ActionResult Index(Cart cart, string returnUrl = null)
        {
            return View(new CartIndexViewModel { Cart = cart, ReturnUrl = returnUrl });
        }


        public ActionResult AddToCart(Cart cart, int id, string returnUrl)
        {
            var product = db.Goods.GetById(id);
            if (product != null)
            {
                cart.AddItem(product, 1);
            }

            return RedirectToAction("Index", new { returnUrl });
        }


        public ActionResult RemoveFromCart(Cart cart, int id, string returnUrl)
        {
            var itemToRemove = cart.Items.FirstOrDefault(t => t.Good.Id == id);
            if (itemToRemove != null)
                cart.RemoveItem(itemToRemove.Good);
            return RedirectToAction("Index", new { returnUrl });
        }


        public JsonResult IncreaseAmount(Cart cart, int id)
        {
            var itemToUpdate = cart.Items.FirstOrDefault(t => t.Good.Id == id);
            if (itemToUpdate != null)
                itemToUpdate.Count += 1;

            return Json(new { count = itemToUpdate.Count });
        }


        public JsonResult DecreaseAmount(Cart cart, int id)
        {
            var itemToUpdate = cart.Items.FirstOrDefault(t => t.Good.Id == id);
            if (itemToUpdate != null && itemToUpdate.Count > 1)
                itemToUpdate.Count -= 1;

            return Json(new { count = itemToUpdate.Count });
        }


        public int GetItemsCount(Cart cart)
        {
            return cart != null ? cart.Items.Count() : 0;
        }
    }
}