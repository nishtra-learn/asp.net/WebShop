﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShopWebApp.Domain.Interfaces;
using ShopWebApp.Domain.Models;
using ShopWebApp.Models;
using ShopWebApp.Utilities;


namespace ShopWebApp.Controllers
{
    public class CategoriesController : Controller
    {
        private IUnitOfWork db;
        private IUoFRepository<Category> categoriesRepo;
        
        public CategoriesController(IUnitOfWork dbUnit)
        {
            this.db = dbUnit;
            this.categoriesRepo = db.Categories;
        }

        // GET: Categories
        public ActionResult Index(int page = 1)
        {
            var itemsPerPage = 2;
            var catList = categoriesRepo.GetList();
            var pageInfo = new PaginationInfo { 
                CurrentPage = page, 
                ItemsCount = catList.Count(), 
                ItemsPerPage = itemsPerPage 
            };
            var categoriesOnPage = catList
                .Skip((page - 1) * itemsPerPage)
                .Take(itemsPerPage);
            var viewModel = new PagedIndexViewModel<Category> {
                Collection = categoriesOnPage,
                PaginationInfo = pageInfo
            };

            return View(viewModel);
        }

        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            Category category = categoriesRepo.GetById(id.Value);
            
            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CategoryName")] Category category)
        {
            if (ModelState.IsValid)
            {
                categoriesRepo.Create(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Category category = categoriesRepo.GetById(id.Value);

            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CategoryName")] Category category)
        {
            if (ModelState.IsValid)
            {
                categoriesRepo.Update(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Category category = categoriesRepo.GetById(id.Value);

            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            categoriesRepo.Delete(id);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
