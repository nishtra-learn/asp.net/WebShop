﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ShopWebApp.Domain.Interfaces;
using ShopWebApp.Domain.Models;
using ShopWebApp.Models;
using ShopWebApp.Utilities;


namespace ShopWebApp.Controllers
{
    public class GoodsController : Controller
    {
        private IUnitOfWork db;
        private IUoFRepository<Category> categoriesRepo;
        private IUoFRepository<Good> goodsRepo;

        public GoodsController(IUnitOfWork dbUnit)
        {
            this.db = dbUnit;
            this.goodsRepo = db.Goods;
            this.categoriesRepo = db.Categories;
        }

        // GET: Goods
        public ActionResult Index(int? page = 1, int? maxResults = null, GoodsFilters filters = null)
        {
            if (page == null || page < 1)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (maxResults < 1)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var itemsPerPage = maxResults ?? PaginationInfo.ItemsPerPageDefault;
            var goodsList = filters.ApplyFilters(goodsRepo.GetList());

            // config pagination helper
            var pageinfo = new PaginationInfo {
                CurrentPage = page.Value,
                ItemsCount = goodsList.Count(),
                ItemsPerPage = itemsPerPage
            };

            var goodsOnPage = goodsList
                .Skip((page.Value - 1) * itemsPerPage)
                .Take(itemsPerPage);

            // config view model
            var viewModel = new FilteredGoodsIndexViewModel {
                PaginationInfo = pageinfo,
                Collection = goodsOnPage,
                CategoriesList = categoriesRepo.GetList(),
                Filters = filters
            };

            return View(viewModel);
        }

        // GET: Goods/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Good good = goodsRepo.GetById(id.Value);

            if (good == null)
            {
                return HttpNotFound();
            }

            return View(good);
        }

        // GET: Goods/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(categoriesRepo.GetList(), "Id", "CategoryName");
            return View();
        }

        // POST: Goods/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Price,CategoryId")] Good good)
        {
            if (ModelState.IsValid)
            {
                goodsRepo.Create(good);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(categoriesRepo.GetList(), "Id", "CategoryName", good.CategoryId);
            return View(good);
        }

        // GET: Goods/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Good good = goodsRepo.GetById(id.Value);

            if (good == null)
            {
                return HttpNotFound();
            }

            ViewBag.CategoryId = new SelectList(categoriesRepo.GetList(), "Id", "CategoryName", good.CategoryId);
            return View(good);
        }

        // POST: Goods/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Price,CategoryId")] Good good)
        {
            if (ModelState.IsValid)
            {
                goodsRepo.Update(good);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(categoriesRepo.GetList(), "Id", "CategoryName", good.CategoryId);
            return View(good);
        }

        // GET: Goods/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Good good = goodsRepo.GetById(id.Value);

            if (good == null)
            {
                return HttpNotFound();
            }

            return View(good);
        }

        // POST: Goods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            goodsRepo.Delete(id);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpPost]
        public ActionResult ApplyFilter(int? itemsPerPage, GoodsFilters filters)
        {
            var routeValues = new RouteValueDictionary();

            if (itemsPerPage != null)
                routeValues.Add("maxResults", itemsPerPage.Value);
            
            // serialize filter collections
            filters.SerializeCollectionFilters();
            if (filters.SelectedCategories.Count > 0)
            {
                routeValues.Add("categories", filters.Categories);
            }

            return RedirectToAction(nameof(Index), routeValues);
        }
    }
}
