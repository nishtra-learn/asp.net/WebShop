﻿using ShopWebApp.Domain.Interfaces;
using ShopWebApp.Domain.Models;
using ShopWebApp.Models;
using ShopWebApp.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ShopWebApp.Controllers
{
    public class ClientController : Controller
    {
        private IUnitOfWork db;
        private IUoFRepository<Category> categoriesRepo;
        private IUoFRepository<Good> goodsRepo;

        public ClientController(IUnitOfWork dbUnit)
        {
            this.db = dbUnit;
            this.goodsRepo = db.Goods;
            this.categoriesRepo = db.Categories;
        }

        // GET: Goods
        [Route("", Name = "Default")]
        [Route("page{page:int}", Name = "Default/Page")]
        [Route("{category}", Name = "Default/Category")]
        [Route("{category}/page{page:int}", Name = "Default/Category/Page")]
        public ActionResult Index(
            int? page = 1, 
            int? maxResults = null, 
            GoodsFilters filters = null, 
            string category = null)
        {
            if (page == null || page < 1)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (maxResults < 1)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            // apply filters
            if (filters == null)
                filters = new GoodsFilters();
            var goodsList = filters.ApplyFilters(goodsRepo.GetList());

            // additional filter by category
            if (category != null)
            {
                var categoryId = categoriesRepo.GetList()
                    .Select(c => new { Id = c.Id, Name = HelperMethods.NormalizeCategoryName(c.CategoryName) })
                    .FirstOrDefault(c => String.Compare(c.Name, category, true) == 0)
                    ?.Id;

                if (categoryId.HasValue)
                {
                    filters.SelectedCategories.Add(categoryId.Value);
                    goodsList = goodsList.Where(g => g.CategoryId == categoryId);
                }
            }

            var itemsPerPage = maxResults ?? PaginationInfo.ItemsPerPageDefault;
            
            // config pagination helper
            var pageinfo = new PaginationInfo
            {
                CurrentPage = page.Value,
                ItemsCount = goodsList.Count(),
                ItemsPerPage = itemsPerPage
            };

            var goodsOnPage = goodsList
                .Skip((page.Value - 1) * itemsPerPage)
                .Take(itemsPerPage);

            // config view model
            var viewModel = new FilteredGoodsIndexViewModel
            {
                PaginationInfo = pageinfo,
                Collection = goodsOnPage,
                CategoriesList = categoriesRepo.GetList(),
                Filters = filters
            };

            return View("~/Views/Client/Index.cshtml", viewModel);
        }


        // GET: Goods/Details/5
        [Route("details/{id?}")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Good good = goodsRepo.GetById(id.Value);

            if (good == null)
            {
                return HttpNotFound();
            }

            return View(good);
        }


        [HttpPost]
        public ActionResult ApplyFilter(int? maxResults, GoodsFilters filters)
        {
            var routeValues = new RouteValueDictionary();

            if (maxResults != null)
                routeValues.Add("maxResults", maxResults.Value);

            // serialize filter collections
            filters.SerializeCollectionFilters();
            if (filters.SelectedCategories.Count == 1)
            {
                var filterCategoryId = filters.SelectedCategories.First();
                var categoryName = categoriesRepo.GetList()
                            .First(c => c.Id == filterCategoryId)
                            .CategoryName;
                var normalizedCategoryName = HelperMethods.NormalizeCategoryName(categoryName);
                routeValues.Add("category", normalizedCategoryName);
                
                var url = Url.RouteUrl("Default/Category", routeValues);

                return Redirect(url);
            }
            else if (filters.SelectedCategories.Count > 1)
            {
                routeValues.Add("categories", filters.Categories);
            }

            return RedirectToAction(nameof(Index), routeValues);
        }
        
    }
}