﻿using ShopWebApp.Domain.Interfaces;
using ShopWebApp.Domain.Models;
using ShopWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopWebApp.Controllers
{
    public class SearchController : Controller
    {
        private IUnitOfWork db;


        public SearchController(IUnitOfWork dbUnit)
        {
            db = dbUnit;
        }


        public ActionResult Search(string q)
        {
            List<Good> searchRes = Find(q);
            return View(searchRes);
        }


        public ActionResult SearchAjax(string q)
        {
            //List<Good> searchRes = Find(q);
            //return View("_SearchResultsTable", searchRes);

            var controller = DependencyResolver.Current.GetService<ClientController>();
            controller.ControllerContext = this.ControllerContext;
            var filter = new GoodsFilters();
            filter.q = q;
            var res = controller.Index(filters: filter) as ViewResult;

            return PartialView("~/Views/Client/_GoodsList.cshtml", res.Model);
        }


        private List<Good> Find(string q)
        {
            List<Good> searchRes = null;
            if (String.IsNullOrWhiteSpace(q))
                searchRes = new List<Good>();
            else
            {
                var searchTokens = q.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                searchRes = db.Goods.GetList()
                    .Where(t => {
                        foreach (var token in searchTokens)
                        {
                            if (!t.Title.ToLower().Contains(token.ToLower()))
                            {
                                return false;
                            }
                        }
                        return true;
                    })
                    .ToList();
            }

            return searchRes;
        }
    }
}