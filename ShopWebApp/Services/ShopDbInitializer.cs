﻿using ShopWebApp.Domain.DataAccess;
using ShopWebApp.Domain.Models;
using ShopWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ShopWebApp.Services
{
    public class ShopDbInitializer : DropCreateDatabaseIfModelChanges<ShopContext>
    {
        protected override void Seed(ShopContext context)
        {
            base.Seed(context);

            var categories = new List<Category>
            {
                new Category{ CategoryName = "Smartphones"},
                new Category{ CategoryName = "TV"},
                new Category{ CategoryName = "Computer parts"},
                new Category{ CategoryName = "Fridge"},
                new Category{ CategoryName = "Laptops"}
            };
            context.Categories.AddRange(categories);
            context.SaveChanges();

            var goods = new List<Good>
            {
                new Good { Title = "Xiaomi Redmi Note 5", Category = categories[0], Price = 5000 },
                new Good { Title = "Xiaomi Redmi 8", Category = categories[0], Price = 6500 },
                new Good { Title = "Xiaomi Redmi 9", Category = categories[0], Price = 7000 },
                new Good { Title = "Samsung Galaxy M51", Category = categories[0], Price = 9000 },
                new Good { Title = "Samsung UE50NU7002UXUA", Category = categories[1], Price = 12000 },
                new Good { Title = "LG 32LM6300PLA", Category = categories[1], Price = 7300 },
                new Good { Title = "BOSCH KGN39VI306", Category = categories[3], Price = 17400 },
                new Good { Title = "Dell Inspiron 3582", Category = categories[4], Price = 9500 },
                new Good { Title = "Lenovo IdeaPad S340-15IWL", Category = categories[4], Price = 16000 },
                new Good { Title = "Dell G3 15 3590", Category = categories[4], Price = 33000 },
                new Good { Title = "MSI Modern 14", Category = categories[4], Price = 23000 },
                new Good { Title = "MSI PCI-Ex GeForce GTX 1660", Category = categories[2], Price = 8000 },
                new Good { Title = "Asus PCI-Ex Radeon RX 580", Category = categories[2], Price = 6850 },
                new Good { Title = "HyperX DDR4-3000 8192MB PC4-24000 Predator RGB Black", Category = categories[2], Price = 1400 },
                new Good { Title = "HyperX DDR4-3200 8192MB PC4-25600 Fury Black", Category = categories[2], Price = 1130 },
                new Good { Title = "Crucial DDR4-3000 16384MB PC4-24000 (Kit of 2x8192) Ballistix Black", Category = categories[2], Price = 2100 },
                new Good { Title = "Asus TUF B450-Pro Gaming", Category = categories[2], Price = 3130 },
                new Good { Title = "MSI B450 Tomahawk Max", Category = categories[2], Price = 3500 }
            };
            context.Goods.AddRange(goods);
            context.SaveChanges();
        }
    }
}