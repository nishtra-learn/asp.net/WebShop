﻿using Autofac;
using Autofac.Integration.Mvc;
using ShopWebApp.Domain.DataAccess;
using ShopWebApp.Domain.Interfaces;
using ShopWebApp.Domain.Models;
using ShopWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopWebApp.Services
{
    public static class AutofacConfig
    {
        public static void SetDependencyResolver()
        {
            var builder = new Autofac.ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            //builder.RegisterType<GoodsRepository>()
            //    .As<IRepository<Good>>()
            //    .WithParameter("connectionString", "ShopDb");
            //builder.RegisterType<CategoriesRepository>()
            //    .As<IRepository<Category>>()
            //    .WithParameter("connectionString", "ShopDb");
            //builder.RegisterType<CommentsRepository>()
            //    .As<IRepository<Comment>>()
            //    .WithParameter("connectionString", "ShopDb");
            builder.RegisterType<UnitOfWorkEF>()
                .As<IUnitOfWork>()
                .WithParameter("connectionString", "ShopUoF");

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}