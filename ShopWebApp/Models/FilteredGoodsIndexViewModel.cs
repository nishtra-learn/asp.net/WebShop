﻿using ShopWebApp.Domain.Models;
using ShopWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApp.Models
{
    public class FilteredGoodsIndexViewModel : PagedIndexViewModel<Good>
    {
        public IEnumerable<Category> CategoriesList { get; set; }
        public GoodsFilters Filters { get; set; }


        public FilteredGoodsIndexViewModel()
        {
            CategoriesList = new List<Category>();
        }
    }
}