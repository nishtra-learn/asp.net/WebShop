﻿using ShopWebApp.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.DynamicData;

namespace ShopWebApp.Models
{
    public class GoodsFilters
    {
        public List<int> SelectedCategories { get; set; }
        /**<summary>
         * Serialized reperesentation of <see cref="SelectedCategories"/>
         * </summary>*/
        public string Categories { get; set; }
        /**<summary>
         * Search query reperesentation of <see cref="SelectedCategories"/>
         * </summary>*/
        public string q { get; set; }


        public GoodsFilters()
        {
            SelectedCategories = new List<int>();
        }


        public IEnumerable<Good> ApplyFilters(IEnumerable<Good> inputCollection)
        {
            DeserializeCollectionFilters();

            var filterRes = inputCollection;
            if (!String.IsNullOrWhiteSpace(q))
            {
                var searchTokens = q.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                filterRes = filterRes.Where(t => {
                    foreach (var token in searchTokens)
                    {
                        if (!t.Title.ToLower().Contains(token.ToLower()))
                        {
                            return false;
                        }
                    }
                    return true;
                });
            }

            if (SelectedCategories.Count > 0)
            {
                filterRes = filterRes.Where(g => SelectedCategories.Contains(g.CategoryId));
            }

            return filterRes;
        }


        /**<summary>
         * Serialize collection-type filters to plain strings and assign them to corresponding properties
         * </summary>*/
        public void SerializeCollectionFilters()
        {
            if (SelectedCategories.Count > 0)
            {
                var serializedCategories = String.Join(",", SelectedCategories);
                Categories = serializedCategories;
            }
        }


        /**<summary>
         * Deserialize collection-type filters and assign them to corresponding properties
         * </summary>*/
        public void DeserializeCollectionFilters()
        {
            if (!String.IsNullOrWhiteSpace(Categories))
            {
                SelectedCategories = Categories
                    .Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                    .SelectMany(t => {
                        bool parseSuccess = int.TryParse(t, out int parsed);
                        if (parseSuccess)
                            return new int[] { parsed };
                        else
                            return new int[] { };
                    })
                    .ToList();
            }
        }
    }
}