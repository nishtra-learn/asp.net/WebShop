﻿using ShopWebApp.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApp.Models
{
    public class PagedIndexViewModel<T>
    {
        public PaginationInfo PaginationInfo { get; set; }
        public IEnumerable<T> Collection { get; set; }
    }
}