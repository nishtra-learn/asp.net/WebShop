﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApp.Utilities
{
    public class PaginationInfo
    {
        public static int ItemsPerPageDefault { get; private set; } = 5;

        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int ItemsCount { get; set; }
        public int PageCount
        {
            get
            {
                return (int)Math.Ceiling(ItemsCount / (double)ItemsPerPage);
            }
        }
    }
}