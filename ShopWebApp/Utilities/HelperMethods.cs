﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopWebApp.Utilities
{
    public static class HelperMethods
    {
        public static string NormalizeCategoryName(string category)
        {
            return category.Replace(" ", "_").ToLower();
        }
    }
}